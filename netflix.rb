require 'mechanize'
require 'json'

class Netflix

  attr_accessor :email, :password

  def initialize(email, password, profile)
    @agent = Mechanize.new
    @email = email
    @password = password
    @profile = profile
    @login_url = '/login/'
    @browse_url = '/browse/'
    @base = 'http://netflix.com'
    @my_list = '/my-list'
    authenticate
    select_profile
  end

  def authenticate
    if File.exist?("cookies.yaml")
      @agent.cookie_jar.load("cookies.yaml")
    else
      login_page = @agent.get @base + @login_url
      login_form = login_page.forms.first
      login_form.field_with(:name => "email").value = @email
      login_form.field_with(:name => "password").value = @password
      login_form.submit
      @agent.cookie_jar.save("cookies.yaml")
    end
  end

  #Select the user profile for netflix user
  def select_profile
    profile_page = @agent.get @base + @browse_url
    profile_link = profile_page.link_with(:text => @profile)
    profile_link.click
  end

  def get_all_links_from_browse_page
    browse_page = @agent.get @base + @browse_url
    all_links = browse_page.search("@href").map(&:text)
    all_links.each do |link|
      puts link.to_s
    end
  end

  # Return a json object with details of all items in the my-list page.
  # Currently there is something wrong with the method
  # It returns a json object but the movie names are wrapped around two lists instead of one
  def get_my_list
    my_list_page = @agent.get @base + @browse_url + @my_list
    movie_names = my_list_page.search('.ptrack-content').map(&:text)
    movie_hash = movie_names.collect { |movie| {'Name' => movie}}
    movies = Hash.new{|hash,key| hash[key] = [] }
    movies['My List'].push movie_hash
    movies.to_json
  end
end
